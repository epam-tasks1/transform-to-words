﻿using System;

#pragma warning disable CA1822

namespace TransformToWordsTask
{
    /// <summary>s
    /// Provides transformer methods.
    /// </summary>
    public sealed class Transformer
    {
        /// <summary>
        /// Converts number's digital representation into words.
        /// </summary>
        /// <param name="number">Number to convert.</param>
        /// <returns>Words representation.</returns>
        public string TransformToWords(double number)
        {
            if (number == double.Epsilon)
            {
                return "Double Epsilon";
            }

            if (double.IsNaN(number))
            {
                return "NaN";
            }

            if (double.IsPositiveInfinity(number))
            {
                return "Positive Infinity";
            }

            if (double.IsNegativeInfinity(number))
            {
                return "Negative Infinity";
            }

            string numberString = number.ToString(System.Globalization.CultureInfo.CurrentCulture);
            string resultString = this.ConvertNumberToWords(numberString);

            return this.CorrectingString(resultString);
        }

        public string CorrectingString(string value)
        {
            return string.Concat(value[0..1].ToUpper(System.Globalization.CultureInfo.CurrentCulture), value[1..^1]);
        }

        public string ConvertNumberToWords(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            string result = string.Empty;

            for (int i = 0; i < value.Length; i++)
            {
                switch (value[i .. (i + 1)])
                {
                    case "-":
                        result = string.Concat(result, "minus ");
                        break;

                    case "+":
                        result = string.Concat(result, "plus ");
                        break;

                    case ".":
                        result = string.Concat(result, "point ");
                        break;

                    case "1":
                        result = string.Concat(result, "one ");
                        break;

                    case "2":
                        result = string.Concat(result, "two ");
                        break;

                    case "3":
                        result = string.Concat(result, "three ");
                        break;

                    case "4":
                        result = string.Concat(result, "four ");
                        break;

                    case "5":
                        result = string.Concat(result, "five ");
                        break;

                    case "6":
                        result = string.Concat(result, "six ");
                        break;

                    case "7":
                        result = string.Concat(result, "seven ");
                        break;

                    case "8":
                        result = string.Concat(result, "eight ");
                        break;

                    case "9":
                        result = string.Concat(result, "nine ");
                        break;

                    case "0":
                        result = string.Concat(result, "zero ");
                        break;

                    case "E":
                        result = string.Concat(result, "E ");
                        break;
                }
            }

            return result;
        }
    }
}
